# ANDROID ARCHITECTURE COMPONENT MODULE #

Module này được thiết kế nhằm tối ưu thời gian phát triển những ứng dụng android.
Giảm chi phí phát sinh và giảm số lỗi không mong muốn trong quá trình phát triển sản phẩm. Module tích hợp những công nghệ mới của android, phù hợp với mọi nhu 
cầu, mọi ứng dụng đặc thù.

### 1. Thông tin về module ###
-----------------------------------------
* Phát triển cho ứng dụng Native Android
* Hỗ trợ nền tảng: Lolliop  - Mới Nhất
* Ngôn ngữ: Java
* Java Version: JavaVersion.VERSION_1_8
* Phiên bản: 1.0.0

### 2. Sử dụng một số third parties ###
-----------------------------------------
* [**Gson**](https://github.com/google/gson): Convert cây Json sang đối tượng Java. 
* [**Retrofit2**](https://square.github.io/retrofit/): Third party giúp quản lý HTTP Connection trong ứng dụng. 
* [**RxJava2**](https://github.com/ReactiveX/RxJava): Third party giúp quản lý bất đồng bộ trong ứng dụng. 
* [**OkHttp3**](https://github.com/square/okhttp): Third party giúp quản lý logging Connection trong ứng dụng. 
* [**Android Architecture Component**](https://developer.android.com/topic/libraries/architecture/): Mô hình MVVM của google phát triển. Được giới thiệu trong GoogleIO năm 2017.
* [**SDP - a scalable size unit**](https://github.com/intuit/sdp): Bộ dimensions hỗ trợ phát triển nhiều kích thước view khác nhau cho từng màn hình khác nhau..
* [**SSP - a scalable size unit for texts**](https://github.com/intuit/ssp): Bộ diêmnsions hỗ trợ phát triển nhiều kích thước chữ khác nhau cho từng màn hình khác nhau.

### 3. Hướng dẫn cấu hình ###
-----------------------------------------
* Bước 1: Mở terminal trong android studio
* Bước 2: Gõ lệnh **git submodule init**
* Bước 3: Gõ lệnh **git submodule add** [https://gitlab.com/username/Support](https://gitlab.com/duyph/Support) **support**. Trong đó từ khoá support là tên module(có thể đặt tên tuỳ ý), username là tên user của bạn trên gitlab.
* Bước 4: Mở file **setting.gradle** và thêm đoạn mã sau: **":support"**. Note: Đặt giống tên đã đặt ở bước 3 giống như bên dưới:
```javascript
include ':app',':support'
```
* Bước 5: Mở app.gradle và thêm đoạn mã sau: **implementation project(path: ':support')**.
* Bước 6: Click vào **file --> Invalidate caches/Restart...** để thực hiện indexing và generate những file còn thiếu.


### 4. Documents ###
-----------------------------------------
> Đang cập nhật


### 5. License ###
-----------------------------------------
Các third parties được sử dụng trong module đều nằm ở APACHE hoặc MIT License. Thông tin tham khảo:
* Apache License: https://en.wikipedia.org/wiki/Apache_License
* MIT License: https://en.wikipedia.org/wiki/MIT_License
