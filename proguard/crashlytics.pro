# Crashlytics 2.+
# Please uncommnent if you want to use


#-keep class com.crashlytics.** { *; }
#-keep class com.crashlytics.android.**
-keepattributes SourceFile, LineNumberTable, *Annotation*

# If you are using custom exceptions, add this line so that custom exception types are skipped during obfuscation:
-keep public class * extends java.lang.Exception