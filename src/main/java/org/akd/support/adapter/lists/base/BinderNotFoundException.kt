package org.akd.support.adapter.lists.base


internal class BinderNotFoundException(clazz: Class<*>) : RuntimeException(
  "Have you registered the ${clazz.name} type and its binder to the adapter or types?"
)
