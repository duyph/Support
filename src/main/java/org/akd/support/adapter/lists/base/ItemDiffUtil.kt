package org.akd.support.adapter.lists.base

import androidx.recyclerview.widget.DiffUtil

import org.akd.support.model.IFlexibleItem

class ItemDiffUtil : DiffUtil.ItemCallback<IFlexibleItem>() {

    override fun areItemsTheSame(oldItem: IFlexibleItem, newItem: IFlexibleItem): Boolean {
        return oldItem.areItemsTheSame(newItem)
    }

    override fun areContentsTheSame(oldItem: IFlexibleItem, newItem: IFlexibleItem): Boolean {
        return oldItem.areContentsTheSame(newItem)
    }
}
