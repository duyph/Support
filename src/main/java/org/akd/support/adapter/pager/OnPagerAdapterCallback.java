package org.akd.support.adapter.pager;


import androidx.fragment.app.Fragment;

import org.akd.support.model.TabItem;

public interface OnPagerAdapterCallback {
    Fragment getItem(TabItem item, int position);
}
