package org.akd.support.bus

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlin.reflect.KClass

internal class EventLiveData(
    private val tag: String,
    private val clazz: KClass<*>
) : LiveData<Event<Any>>() {

    fun update(obj: Event<Any>) {
        postValue(obj)
    }

    override fun removeObserver(observer: Observer<in Event<Any>>) {
        super.removeObserver(observer)
        if (!hasObservers()) {
            Log.i("EventLiveData", "No one listened to me, so I gave up. tag=$tag, context=$clazz")
            LiveDataBus.unregister(tag, clazz)
        }
    }
}