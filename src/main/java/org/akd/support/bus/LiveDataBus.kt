package org.akd.support.bus

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import kotlin.reflect.KClass

object LiveDataBus {
    private val mLiveDataStores: MutableList<LiveDataWrapper> = mutableListOf()

    private fun getLiveData(clazz: KClass<*>, tag: String): EventLiveData {
        var liveData = mLiveDataStores.find {
            val isSameTag = it.tag == tag
            val isSameContext = clazz == it.clazz
            isSameTag && isSameContext
        }?.liveData
        if (liveData == null) {
            liveData = EventLiveData(tag, clazz)
            mLiveDataStores.add(LiveDataWrapper(clazz, tag, liveData))
        }
        return liveData
    }


    fun <T : Any> subscribe(
        tag: String,
        owner: LifecycleOwner,
        block: (data: T) -> Unit = {}
    ) {
        try {
            val clazz = owner::class
            getLiveData(clazz, tag).observe(owner, Observer {
                if (owner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                    val data = it.getContentIfNotHandled()
                    data?.let { @Suppress("UNCHECKED_CAST") block(data as T) }
                }
            })
        } catch (throwable: IllegalArgumentException) {
            throwable.printStackTrace()
        }
    }

    fun unregister(tag: String, clazz: KClass<*>) {
        val items = findLiveDataByTag(tag, clazz)
        mLiveDataStores.removeAll(items)
    }

    private fun findLiveDataByTag(tag: String, clazz: KClass<*>? = null): List<LiveDataWrapper> {
        return mLiveDataStores.filter { it.tag == tag && if (clazz != null) it.clazz == clazz else true }
            .toList()
    }

    fun post(tag: String, message: Any) {
        val wrappers = findLiveDataByTag(tag)
        wrappers.forEach {
            it.liveData.update(Event(message))
        }
    }


    private data class LiveDataWrapper(
        val clazz: KClass<*>,
        val tag: String,
        val liveData: EventLiveData
    )
}