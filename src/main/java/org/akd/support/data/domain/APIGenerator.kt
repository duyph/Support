package org.akd.support.data.domain


import android.content.Context
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.akd.support.data.domain.interceptor.ConnectivityInterceptor
import org.akd.support.helper.JsonGenerator
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

open class APIGenerator(context: Context, baseUrl: String) {
    private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val httpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
    private val retrofitBuilder: Retrofit.Builder
    private val mWeakReference: WeakReference<Context> = WeakReference(context)


    init {
        retrofitBuilder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create(setMoshiConfig()))
    }

    open fun setMoshiConfig(): Moshi {
        return JsonGenerator.moshi
    }

    open fun addRetrofitBuilder(): Retrofit.Builder {
        retrofitBuilder.client(addOkHttpBuilder().build())
        return retrofitBuilder
    }

    open fun addOkHttpBuilder(): OkHttpClient.Builder {
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClientBuilder.readTimeout(configTimeout().toLong(), TimeUnit.SECONDS)
        httpClientBuilder.connectTimeout(configTimeout().toLong(), TimeUnit.SECONDS)
        httpClientBuilder.addInterceptor(logging)
        httpClientBuilder.addInterceptor(ConnectivityInterceptor(mWeakReference.get()))
        return httpClientBuilder
    }

    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = addRetrofitBuilder().build()
        return retrofit.create(serviceClass)
    }

    protected fun configTimeout() = 30


}
