package org.akd.support.data.domain


import java.util.*
import kotlin.reflect.KClass

object APIManager {
    private val mCachedAPIManager: HashMap<Class<*>, APIGenerator> = hashMapOf()

    fun addGenerator(generator: APIGenerator) {
        if (mCachedAPIManager.containsKey(generator.javaClass)) return
        mCachedAPIManager[generator.javaClass] = generator
    }

    fun removeGenerator(generator: APIGenerator) {
        mCachedAPIManager.remove(generator.javaClass)
    }

    inline operator fun <reified T : APIGenerator> get(clazz: KClass<T>): T = get(clazz.java)

    operator fun <T : APIGenerator> get(clazz: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        if (APIGenerator::class.java.isAssignableFrom(clazz))
            return mCachedAPIManager[clazz] as T
        throw IllegalArgumentException("You not register this class: "
                + clazz.simpleName + ", please register and try again!")
    }
}
