package org.akd.support.data.domain.interceptor

import android.content.Context

import org.akd.support.exception.NoInternetException

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Response
import org.akd.support.util.network.NetworkMonitors

class ConnectivityInterceptor(private val mContext: Context?) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val hasInternetOrNot = mContext != null && NetworkMonitors.getInstance(mContext).isConnected
        if (!hasInternetOrNot) {
            throw NoInternetException()
        }
        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}
