package org.akd.support.exception

import java.io.IOException

class NoInternetException : IOException() {

    override val message: String?
        get() = "No network connection"
}
