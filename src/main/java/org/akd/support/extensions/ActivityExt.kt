package org.akd.support.extensions

import android.graphics.Color
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import org.akd.support.R
import org.akd.support.widget.dialog.SweetAlertDialog

fun FragmentActivity.setFullscreen() {
    window.decorView.systemUiVisibility =
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
    setUpTransparentStatusBar()
}

fun FragmentActivity.clearFullScreen() {
    window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    window?.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
    window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window?.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
}

private fun FragmentActivity.setWindowFlag(bits: Int, on: Boolean) {
    val win = window
    val winParams = win.attributes
    if (on) {
        winParams.flags = winParams.flags or bits
    } else {
        winParams.flags = winParams.flags and bits.inv()
    }
    win.attributes = winParams
}

fun FragmentActivity.setUpStatusBar(color: Int) {
    window.statusBarColor = ContextCompat.getColor(this, color)
}

fun FragmentActivity.setUpTransparentStatusBar() {
    window.statusBarColor = Color.TRANSPARENT
}

val FragmentManager.currentNavigationFragment: Fragment?
    get() = primaryNavigationFragment?.childFragmentManager?.fragments?.first()

/**
 * dialog.setContentText(mes)
 *       .setTitleText(R.string.msg_notification)
 *       .setCancelText(getString(R.string.cancel))
 *       .setConfirmText(getString(R.string.ok))
 *       .showCancelButton(true)
 *       .setConfirmClickListener(event)
 *       .setCancelClickListener(SweetAlertDialog::cancel)
 */
fun FragmentActivity.showDialog(block: SweetAlertDialog.() -> Unit): SweetAlertDialog {
    val dialog = SweetAlertDialog(this)
    dialog.apply(block)
    dialog.show()
    return dialog
}

