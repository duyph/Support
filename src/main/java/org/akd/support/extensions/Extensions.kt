package org.akd.support.extensions

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.squareup.moshi.Types
import org.akd.support.helper.JsonGenerator
import java.io.Serializable

fun <T> LiveData<T>.observe(owner: LifecycleOwner, block: (data: T) -> Unit) =
    observe(owner, Observer { it?.also(block) })

fun Any.toJson() = JsonGenerator.toJson(this)
fun <T> mutableLiveDataOf() = MutableLiveData<T>()
inline fun <reified T : Any> List<T>.toJson() = JsonGenerator.toListJson(this)
inline fun <reified T : Any> String.fromJson() = JsonGenerator.fromJson<T>(this, T::class.java)
inline fun <reified T : Any> String.fromListJson(): List<T> = JsonGenerator.fromListJson(this)


interface BundleBuilder {
    infix fun String.bundleTo(value: Int)
    infix fun String.bundleTo(value: String)
    infix fun String.bundleTo(value: Boolean)
    infix fun String.bundleTo(value: Double)
    infix fun String.bundleTo(value: Long)
    infix fun String.bundleTo(value: Parcelable)
    infix fun String.bundleTo(value: Serializable)
}

class BundleBuilderImpl(val bundle: Bundle) : BundleBuilder {
    override fun String.bundleTo(value: Int) = bundle.putInt(this, value)
    override fun String.bundleTo(value: String) = bundle.putString(this, value)
    override fun String.bundleTo(value: Boolean) = bundle.putBoolean(this, value)
    override fun String.bundleTo(value: Double) = bundle.putDouble(this, value)
    override fun String.bundleTo(value: Long) = bundle.putLong(this, value)
    override fun String.bundleTo(value: Parcelable) = bundle.putParcelable(this, value)
    override fun String.bundleTo(value: Serializable) = bundle.putSerializable(this, value)
}

fun bundle(arg: Bundle = Bundle(), block: BundleBuilder.() -> Unit): Bundle {
    val bundle = BundleBuilderImpl(arg)
    bundle.block()
    return bundle.bundle
}

fun String.toast(context: Context?, length: Int = Toast.LENGTH_SHORT) {
    context?.let { Toast.makeText(it, this, length).show() }
}

fun Any.asMap(): Map<String, String> {
    val json = JsonGenerator.toJson(this)
    val type = Types.newParameterizedType(
        MutableMap::class.java,
        String::class.java,
        String::class.java
    )
    return json?.let { JsonGenerator.fromJson<Map<String, String>>(it, type) } ?: mapOf()
}