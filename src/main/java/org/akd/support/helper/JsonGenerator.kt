package org.akd.support.helper

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

object JsonGenerator {
    @Suppress("SpellCheckingInspection")
    val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    fun toJson(any: Any): String? {
        val adapter = moshi.adapter(Any::class.java)
        return adapter.toJson(any)
    }

    inline fun <reified T : Any> toListJson(items: List<T>): String? {
        val type = Types.newParameterizedType(List::class.java, T::class.java)
        val adapter = moshi.adapter<List<T>>(type)
        return adapter.toJson(items)
    }

    inline fun <reified T : Any> fromListJson(json: String): List<T> {
        val type = Types.newParameterizedType(List::class.java, T::class.java)
        val adapter = moshi.adapter<List<T>>(type)
        return adapter.fromJson(json) ?: mutableListOf()
    }

    private fun mayBeJSON(string: String?): Boolean {
        return string != null && ("null" == string
                || string.startsWith("[") && string.endsWith("]")
                || string.startsWith("{") && string.endsWith("}"))
    }

    fun <T> fromJson(json: String, type: Type): T? {
        try {
            if (mayBeJSON(json)) {
                val adapter = moshi.adapter<T>(type)
                return adapter.fromJson(json)
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return null
    }
}
