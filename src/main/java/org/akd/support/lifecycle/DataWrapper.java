package org.akd.support.lifecycle;

public class DataWrapper<T> {
    private T data;
    private Throwable throwable;

    public DataWrapper(T data, Throwable throwable) {
        this.data = data;
        this.throwable = throwable;
    }

    public DataWrapper(T data) {
        this(data, null);
    }

    public boolean hasException() {
        return throwable != null;
    }

    public T getData() {
        return data;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
