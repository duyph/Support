package org.akd.support.lifecycle;


import androidx.lifecycle.MutableLiveData;

public class MutableWrapperLiveData<T> extends MutableLiveData<DataWrapper<T>> {

    public void setWrapperValue(T data) {
        setValue(new DataWrapper<T>(data));
    }

    public void setWrapperError(Throwable throwable) {
        setValue(new DataWrapper<T>(null, throwable));
    }

    public void postWrapperValue(T data) {
        postValue(new DataWrapper<T>(data));
    }

    public void postWrapperValue(Throwable throwable){
        postValue(new DataWrapper<T>(null,throwable));
    }

    @Override
    public final void setValue(DataWrapper<T> value) {
        super.setValue(value);
    }

    @Override
    public final void postValue(DataWrapper<T> value) {
        super.postValue(value);
    }
}
