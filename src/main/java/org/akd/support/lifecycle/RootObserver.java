package org.akd.support.lifecycle;


import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

public abstract class RootObserver<T> implements Observer<DataWrapper<T>> {

    @Override
    public final void onChanged(@Nullable DataWrapper<T> wrapper) {
        if (wrapper!=null){
            if (wrapper.hasException()) onFailure(wrapper.getThrowable());
            else onSuccess(wrapper.getData());
        }else onFailure(new NullPointerException("Not support null wrapper."));
    }

    protected abstract void onSuccess(T data);

    protected void onFailure(Throwable e){
        e.printStackTrace();
    }
}
