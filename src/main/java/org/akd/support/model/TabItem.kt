package org.akd.support.model

import androidx.annotation.DrawableRes
import com.squareup.moshi.Json

data class TabItem(
        @Json(name = "id") val id: Int,
        @Json(name = "name") val name: String,
        @DrawableRes @Json(name = "resourceIcon") val resourceIcon: Int,
        @Json(name = "content") val content: String)
