package org.akd.support.util;

import android.content.Context;
import android.text.Html;

import org.akd.support.R;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Pattern;

public class Formatter {

    private static final String DEF_DURATION = "00:00";
    private static final long TWO_DAYS = 2 * 24 * 60 * 60 * 1000;
    private static final long ONE_DAY = 24 * 60 * 60 * 1000;
    private static final long ONE_HOUR = 60 * 60 * 1000;
    private static final long ONE_MINUTE = 60 * 1000;
    private static final NavigableMap<Double, String> suffixes = new TreeMap<>();

    static {
        suffixes.put(1_000.0, "K");
        suffixes.put(1_000_000.0, "M");
        suffixes.put(1_000_000_000.0, "B");
        suffixes.put(1_000_000_000_000.0, "T");
        suffixes.put(1_000_000_000_000_000.0, "P");
        suffixes.put(1_000_000_000_000_000_000.0, "E");
    }

    /**
     * Convert a time to understandable string duration. For example, 69 seconds -> "01:09"
     *
     * @param time the time in second
     * @return the string version for duration type
     */
    public static String formatDuration(int time) {
        if (time <= 0) {
            return "";
        }
        return formatDurationSecond(time);
    }

    /**
     * Convert a time to understandable string duration. For example, 69 seconds -> "01:09"
     *
     * @param time the time in second
     * @return the string version for duration type
     */
    public static String formatDuration(int time, boolean useDefTime) {
        if (time <= 0 && useDefTime) {
            return DEF_DURATION;
        }
        if (time <= 0) {
            return "";
        }
        return formatDurationSecond(time);
    }

    private static String formatDurationSecond(int time) {
        // get minutes
        int minutes = time / 60;
        int second = time % 60;
        String min = minutes > 9 ? String.valueOf(minutes) : String.format(Locale.getDefault(), "0%d", minutes);
        String sec = second > 9 ? String.valueOf(second) : String.format(Locale.getDefault(), "0%d", second);

        return String.format("%1$s:%2$s", min, sec);
    }

    /**
     * Format a unix time to an understandable format for published time.<br>
     * Example: <br>xxxxxxxxxx -> "2 nam truoc"<br>
     * xxxxxxxxxx -> "6 phut truoc"
     *
     * @param time the time (in unit format)
     * @return the String version for published time
     */
    public static String formatShortTime(Context context, long time) {
        long diff = System.currentTimeMillis() - time * 1000;
        // if the different is greater than 2 days, show the date
        if (diff - TWO_DAYS > 0) {
            return formatDate(time * 1000, "dd/MM/yyyy");
        }
        if (diff - ONE_DAY > 0) {
            return context.getString(R.string.yesterday);
        }
        if (diff - ONE_HOUR > 0) {
            return String.format(Locale.getDefault(), context.getString(R.string.hour_ago), diff / ONE_HOUR);
        }
        if (diff - ONE_MINUTE > 0) {
            return String.format(Locale.getDefault(), context.getString(R.string.minutes_ago), diff / ONE_MINUTE);
        }
        return context.getString(R.string.recent);
    }

    /**
     * Format the number to friendly style string
     *
     * @param val the number
     * @return the string version of this number
     */
    public static String formatNumber(double val) {
        return formatNumber(val,2);
    }

    /**
     * Format the number to friendly style string
     *
     * @param val the number
     * @param digits the fraction digits number
     * @return the string version of this number
     */
    public static String formatNumber(double val, int digits) {
        NumberFormat formatter = NumberFormat.getInstance(Locale.getDefault());
        formatter.setMaximumFractionDigits(digits);
        return formatter.format(val);
    }

    public static String percent(double val) {
        NumberFormat formatter = NumberFormat.getPercentInstance();
        formatter.setMaximumFractionDigits(2);
        return formatter.format(val);
    }

    /**
     * Format the number that represents by a string to friendly style string
     *
     * @param val the number
     * @return the string version of this number
     */
    public static String formatNumber(String val) {
        try {
            double numVal = Double.parseDouble(val);
            return NumberFormat.getInstance(new Locale("vi", "VN")).format(numVal);
        } catch (NumberFormatException ignored) {

        }
        return val;
    }

    public static String formatDate(long time, String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(new Date(time));
    }

    /**
     * Convert the number of days to unix time (millisecond value)
     *
     * @param days number of days
     * @return the millisecond value of number of days
     */
    public static long calculateDaysUnixTime(int days) {
        return days * 24L * 60 * 60 * 1000;
    }

    public static String hhmmOnly(String hhmmss) {
        if (hhmmss == null || hhmmss.length() < 5) {
            return hhmmss;
        }
        return hhmmss.substring(0, 5);
    }

    public static long getFirstDayInMonth(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static int getLastDayInMonth(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static int getDayInWeek(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static int getDayInMonth(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static long modifyMonth(long time, int nOM) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.add(Calendar.MONTH, nOM);
        return calendar.getTimeInMillis();
    }

    public static int sameDay(long time, long time1) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int curYear = calendar.get(Calendar.YEAR);
        int curMonth = calendar.get(Calendar.MONTH);
        int curDay = calendar.get(Calendar.DAY_OF_MONTH);

        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time1);
        int timeYear = calendar.get(Calendar.YEAR);
        int timeMonth = calendar.get(Calendar.MONTH);
        int timeDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (curYear < timeYear) {
            return -1;
        } else if (curYear > timeYear) {
            return 1;
        }
        if (curMonth < timeMonth) {
            return -1;
        } else if (curMonth > timeMonth) {
            return 1;
        }
        if (curDay < timeDay) {
            return -1;
        } else if (curDay > timeDay) {
            return 1;
        }
        return 0;
    }

    public static long getCurrentDate() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static String stripMark(String src) {
        if (src == null) {
            return null;
        }
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        String decomposed = Normalizer.normalize(src, Normalizer.Form.NFD);
        return pattern.matcher(decomposed).replaceAll("");
    }

    public static String decodeStr(String raw) {
        if (raw == null) {
            return null;
        }
        try {
            return URLDecoder.decode(raw, "UTF-8");
        } catch (UnsupportedEncodingException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return raw;
    }

    public static String encodeStr(String raw) {
        if (raw == null) {
            return null;
        }
        try {
            return URLEncoder.encode(raw, "UTF-8");
        } catch (UnsupportedEncodingException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return raw;
    }

    public static String encodeStrEscape(String raw) {
        if (raw == null) {
            return null;
        }
        try {
            return URLEncoder.encode(raw, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return raw;
    }

    public static String stripHtml(String raw) {
        if (raw == null) {
            return null;
        }
        return Html.fromHtml(raw).toString();
    }

    public static String convertFormatDate(String value, String oldPattern, String newPattern) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(oldPattern, Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = format.parse(value);
        format.setTimeZone(TimeZone.getDefault());
        format.applyPattern(newPattern);
        return format.format(date);
    }

    /***
     * this method is used to convert number into spec format number
     * @param value number you want to convert
     * @return convert 1000 ->1K
     */
    public static String format(double value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return formatNumber(value); //deal with easy case

        Map.Entry<Double, String> e = suffixes.floorEntry(value);
        double divideBy = e.getKey();
        String suffix = e.getValue();

        double truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? formatNumber((truncated / 10d)) + suffix : formatNumber(truncated / 10f) + suffix;
    }

    public static String formatShortNumber(double value) {
        if (value >= 1000000000) {
            return formatBillion(value);
        }
        if (value >= 1000000) {
            return formatMillion(value);
        }
        if (value >= 1000) {
            return formatThousand(value);
        }
        return format(value);
    }

    private static String formatBillion(double value) {
        double n1 = value / 1000000000;
        double n2 = value % 1000000000 / 1000000;
        if (n2 == 0) {
            return String.format(Locale.getDefault(), "%.0fB", n1);
        }
        return String.format(Locale.getDefault(), "%.0fB %.0fM", n1, n2);
    }

    private static String formatMillion(double value) {
        double n1 = value / 1000000;
        double n2 = value % 1000000 / 1000;
        if (n2 == 0) {
            return String.format(Locale.getDefault(), "%.0fM", n1);
        }
        return String.format(Locale.getDefault(), "%.0fM %.0fK", n1, n2);
    }

    private static String formatThousand(double value) {
        double n1 = value / 1000;
        double n2 = (value % 1000) / 100;
        if (n2 == 0) {
            return String.format(Locale.getDefault(), "%.0fK", n1);
        }
        return String.format(Locale.getDefault(), "%.0fK %.0f", n1, n2);
    }


    public static long getTheDayBefore() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTimeInMillis();
    }

    public static long getLast7Day() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        return cal.getTimeInMillis();
    }

    public static long getLast30Day() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        return cal.getTimeInMillis();
    }

    public static long getLastYear() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -365);
        return cal.getTimeInMillis();
    }
}
