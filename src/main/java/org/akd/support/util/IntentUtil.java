package org.akd.support.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.akd.support.R;

public class IntentUtil {

    public IntentUtil() {
    }

    public Intent intent(String url) {
        return intent(uri(url));
    }

    public Uri uri(String url) {
        return Uri.parse(url);
    }

    public Intent intent(Uri uri) {
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public View.OnClickListener clickUri(@Nullable Context context, Uri uri) {
        return clickIntent(context, intent(uri));
    }

    public View.OnClickListener clickIntent(@Nullable Context context, final Intent intent) {
        return view -> open(context, intent);
    }

    public void open(@Nullable Context context, Intent intent) {
        if (context == null) {
            Log.e("IntentUtil", "Context at open function is null");
            return;
        }
        try {
            context.startActivity(intent);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void open(@Nullable Context context, Uri uri) {
        open(context, intent(uri));
    }

    public Intent openFacebook(@NonNull Context context, String user) {
        try {
            tryPackage(context, R.string.id_facebook_app);
            return intent(context, R.string.uri_facebook_app, user);
        } catch (Exception e) {
            return intent(context, R.string.url_facebook_website, user);
        }
    }

    public Intent intent(@NonNull Context context, int res, String user) {
        return intent(uri(context, res, user));
    }

    public Uri uri(@NonNull Context context, int res, String user) {
        return Uri.parse(context.getString(res, user));
    }

    private void tryPackage(@NonNull Context context, int res) throws PackageManager.NameNotFoundException {
        context.getPackageManager().getPackageInfo(context.getString(res), 0);
    }

    public Intent openInstagram(@NonNull Context context, String user) {
        try {
            tryPackage(context, R.string.id_instagram_app);
            return intent(context, R.string.uri_instagram_app, user);
        } catch (Exception e) {
            return intent(context, R.string.url_instagram_website, user);
        }
    }

    public Intent openTwitter(@NonNull Context context, String user) {
        try {
            tryPackage(context, R.string.id_twitter_app);
            return intent(context, R.string.uri_twitter_app, user);
        } catch (Exception e) {
            return intent(context, R.string.url_twitter_website, user);
        }
    }

    public Intent openGooglePlus(@NonNull Context context, String user) {
        try {
            tryPackage(context, R.string.id_google_plus_app);
            return intent(context, R.string.uri_google_plus_app, user);
        } catch (Exception e) {
            return intent(context, R.string.url_google_plus_website, user);
        }
    }

    public Intent openGooglePlayDev(@NonNull Context context, String user) {
        try {
            return intent(context, R.string.url_google_play_store_developer_page, user);
        } catch (Exception e) {
            return intent(context, R.string.url_google_play_store_developer_page, user);
        }
    }

    public Intent openYoutubeChannel(@NonNull Context context, String user) {
        try {
            return intent(context, R.string.url_youtube_channel_website, user);
        } catch (Exception e) {
            return intent(context, R.string.url_youtube_channel_website, user);
        }
    }

    public Intent openYoutubeUser(@NonNull Context context, String user) {
        try {
            return intent(context, R.string.url_youtube_user_website, user);
        } catch (Exception e) {
            return intent(context, R.string.url_youtube_user_website, user);
        }
    }

    public Intent openLinkedIn(@NonNull Context context, String user) {
        try {
            tryPackage(context, R.string.id_linkedin_app);
            return intent(context, R.string.uri_linkedin_app, user);
        } catch (Exception e) {
            return intent(context, R.string.url_linkedin_website, user);
        }
    }


    public Intent openSkype(@NonNull Context context, String phone) {
        try {
            tryPackage(context, R.string.id_skype_app);
            return intent(context, R.string.uri_skype_app, phone);
        } catch (Exception e) {
            return intent(context, R.string.uri_skype_app, phone);
        }
    }

    public Intent openAddContact(String name, String phone) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);

        return intent;
    }

    public Intent sendEmail(String email, String subject, String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);

        return intent;
    }

    public Intent openPlayStoreAppPage(@NonNull Context context, String app) {
        Intent intent = intent(context, R.string.uri_play_store_app, app);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            return intent;
        } else {
            return intent(context, R.string.uri_play_store_app_website, app);
        }
    }

    public Intent openPlayStoreAppsList(@NonNull Context context, String app) {
        Intent intent = intent(context, R.string.uri_play_store_apps_list, app);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            return intent;
        } else {
            return intent(context, R.string.uri_play_store_apps_list_website, app);
        }
    }

    public Intent shareThisApp(String subject, String message) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        return intent;
    }
}
