package org.akd.support.util;

import android.content.Context;

import java.util.List;

public class Utils {
    /**
     * Check is list valid or not.
     **/
    public static boolean isListValid(List<?> list) {
        return list != null && list.size() > 0;
    }

    /**
     * Convert the dp value to corresponding pixel value
     *
     * @param context the context
     * @param dp      the dp value
     * @return the px value that is corresponding to the dp
     */
    public static float dpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    /**
     * Convert the sp value to corresponding pixel value
     *
     * @param context the context
     * @param sp      the sp value
     * @return the px value that is corresponding to the sp
     */
    public static float spToPx(Context context, float sp) {
        return sp * context.getResources().getDisplayMetrics().scaledDensity;
    }
}
