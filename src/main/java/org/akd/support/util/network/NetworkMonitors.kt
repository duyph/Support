package org.akd.support.util.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import org.akd.support.util.SingletonHolder
import java.lang.ref.WeakReference
import java.util.*

class NetworkMonitors(context: Context) {
    private val mWeakContext: WeakReference<Context?> = WeakReference(context)
    private var mCallbacks: LinkedHashMap<Int, WeakConnectivityListener> = linkedMapOf()

    @Suppress("DEPRECATION")
    val isConnected: Boolean
        get() {
            val context: Context = mWeakContext.get() ?: return false
            val manager = getConnectivityManager(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val nw = manager.activeNetwork ?: return false
                return detectNetworkStatus(nw)
            } else {
                val nwInfo = manager.activeNetworkInfo ?: return false
                return nwInfo.isConnected
            }
        }

    private val mConnectivityCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            val isOnline = detectNetworkStatus(network)
            notifyConnectivityChanged(isOnline)
            Log.d("NetworkMonitors", "onAvailable#notifyConnectivityChanged($isOnline)")
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            val isOnline = detectNetworkStatus(network)
            notifyConnectivityChanged(isOnline)
            Log.d("NetworkMonitors", "onLost#notifyConnectivityChanged($isOnline)")
        }
    }

    fun registerWithLifecycle(owner: LifecycleOwner, callback: OnConnectivityListener) {
        val key = owner::class.hashCode()
        val observer = LifecycleObserver {
            when (it) {
                Lifecycle.Event.ON_RESUME -> register(key, callback)
                Lifecycle.Event.ON_PAUSE -> unregister(key)
                else -> Unit
            }
        }
        owner.lifecycle.addObserver(observer)
        start()
    }

    private fun detectNetworkStatus(network: Network): Boolean {
        val context: Context = mWeakContext.get() ?: return false
        val manager = getConnectivityManager(context)
        val actNw = manager.getNetworkCapabilities(network) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            else -> false
        }
    }

    private fun getConnectivityManager(context: Context): ConnectivityManager {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.getSystemService(ConnectivityManager::class.java)
        } else context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    private fun notifyConnectivityChanged(isOnline: Boolean) {
        if (mCallbacks.size > 0) {
            mCallbacks.map {
                val callback = it.value.get()
                callback?.onConnectivityChanged(isOnline)
            }
        }
    }

    fun unregister(key: Int) {
        mCallbacks.remove(key)
    }

    fun register(key: Int, callback: OnConnectivityListener) {
        unregister(key)
        mCallbacks[key] = WeakConnectivityListener(key, callback)
    }

    private fun start() {
        val context: Context = mWeakContext.get() ?: return
        val manager = getConnectivityManager(context)
        unregisteredNetworkCallbackIfNeeded(manager)
        val request = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            manager.registerDefaultNetworkCallback(mConnectivityCallback)
        } else manager.registerNetworkCallback(request, mConnectivityCallback)
    }

    private fun unregisteredNetworkCallbackIfNeeded(manager: ConnectivityManager) = try {
        manager.unregisterNetworkCallback(mConnectivityCallback)
    } catch (ex: Exception) {
        //ignored
    }

    fun reset() {
        ///reset everything
        mCallbacks.clear()
    }

    internal class LifecycleObserver(
        private val callback: (state: Lifecycle.Event) -> Unit
    ) : androidx.lifecycle.LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onPause() {
            callback(Lifecycle.Event.ON_PAUSE)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onResume() {
            callback(Lifecycle.Event.ON_RESUME)
        }
    }

    companion object : SingletonHolder<NetworkMonitors, Context>({ NetworkMonitors(it) })
}