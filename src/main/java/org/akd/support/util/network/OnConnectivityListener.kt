package org.akd.support.util.network

interface OnConnectivityListener {
    /**
     * This callback maybe called more than once if the network changed
     * @param isOnline true if network is online, false otherwise
     */
    fun onConnectivityChanged(isOnline: Boolean)
}