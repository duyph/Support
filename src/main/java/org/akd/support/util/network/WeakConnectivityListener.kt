package org.akd.support.util.network

import java.lang.ref.WeakReference

class WeakConnectivityListener internal constructor(
    val id: Int,
    callback: OnConnectivityListener
) {
    private val mWeakCallback: WeakReference<OnConnectivityListener> = WeakReference(callback)
    fun get(): OnConnectivityListener? = mWeakCallback.get()

    override fun equals(other: Any?): Boolean {
        return (other is WeakConnectivityListener
                && other.id == id)
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + mWeakCallback.hashCode()
        return result
    }
}