package org.akd.support.util.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import org.akd.support.helper.JsonGenerator.fromJson
import org.akd.support.helper.JsonGenerator.toJson
import java.io.IOException
import java.lang.reflect.Type
import java.security.GeneralSecurityException

class SharePrefUtil(
    private val context: Context,
    private val config: Config
) {
    private var mSharePreference: SharedPreferences? = null

    init {
        try {
            val alias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
            mSharePreference = if (config.isSecure) EncryptedSharedPreferences.create(
                config.preferenceName,
                alias,
                context,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            ) else context.getSharedPreferences(
                config.preferenceName,
                config.preferenceMode
            )
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * Save int value into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    fun saveInt(name: String?, value: Int) {
        mSharePreference?.apply {
            val editor = edit()
            editor.putInt(name, value)
            editor.apply()
        }
    }

    /**
     * Save String value into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    fun saveString(name: String?, value: String?) {
        mSharePreference?.apply {
            val editor = edit()
            editor.putString(name, value)
            editor.apply()
        }
    }

    /**
     * Save Object value into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    fun saveObject(name: String?, value: Any?) {
        if (value == null) return
        mSharePreference?.apply {
            val editor = edit()
            val json = toJson(value)
            editor.putString(name, json)
            editor.apply()
        }
    }

    /**
     * Save boolean into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    fun saveBoolean(name: String?, value: Boolean) {
        mSharePreference?.apply {
            val editor = edit()
            editor.putBoolean(name, value)
            editor.apply()
        }
    }

    /**
     * Save Long into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    fun saveLong(name: String?, value: Long) {
        mSharePreference?.apply {
            val editor = edit()
            editor.putLong(name, value)
            editor.apply()
        }
    }

    /**
     * Read value from temp share pref
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readString(name: String?, def: String?): String? {
        return mSharePreference?.getString(name, def)
    }

    /**
     * read Object value into share preferences
     *
     * @param name the key
     * @param type type of object you want to parse
     */
    @Suppress("UNCHECKED_CAST")
    fun <T> readObject(name: String?, type: Type?): T? {
        return mSharePreference?.let {
            val json: String = it.getString(name, "{}") ?: "{}"
            val data = fromJson<String>(json, type!!)
            return data as T?
        }
    }

    /**
     * Read value from share pref
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readBoolean(name: String?, def: Boolean): Boolean {
        return mSharePreference?.getBoolean(name, def) ?: def
    }

    /**
     * Read value from share pref
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readInt(name: String?, def: Int): Int {
        return mSharePreference?.getInt(name, def) ?: def
    }

    /**
     * Read value from share pref
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readLong(name: String?, def: Long): Long {
        return mSharePreference?.getLong(name, def) ?: def
    }

    /**
     * Read value from temp share pref
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readBooleanTemp(name: String?, def: Boolean): Boolean {
        return mSharePreference?.let {
            val value = it.getBoolean(name, def)
            val editor = it.edit()
            editor.remove(name)
            editor.apply()
            return value
        } ?: def
    }

    /**
     * Read and delete value from temp share preferences
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readIntTemp(name: String?, def: Int): Int {
        return mSharePreference?.let {
            val value = it.getInt(name, def)
            val editor = it.edit()
            editor.remove(name)
            editor.apply()
            return value
        } ?: def
    }

    /**
     * Read and delete value from temp share preferences
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readStringTemp(name: String?, def: String?): String? {
        return mSharePreference?.let {
            val value = it.getString(name, def)
            val editor = it.edit()
            editor.remove(name)
            editor.apply()
            return value
        } ?: def
    }

    /**
     * Read and delete value from temp share preferences
     *
     * @param name the key
     * @param def  the default value when the key is invalid
     * @return the value map to the key
     */
    fun readLongTemp(name: String?, def: Long): Long {
        return mSharePreference?.let {
            val value = it.getLong(name, def)
            val editor = it.edit()
            editor.remove(name)
            editor.apply()
            return value
        } ?: def
    }

    /**
     * remove key from share pref
     *
     * @param name the key
     * @return true if removed, false otherwise
     */
    fun removeKey(name: String?): Boolean {
        return mSharePreference?.let {
            val editor = it.edit()
            editor.remove(name)
            editor.apply()
            return true
        } ?: false
    }

    class Builder(private val context: Context) {
        private var name: String = "default.share_pref"
        private val config: Config = Config(name)

        fun setName(name: String) = apply { config.preferenceName = name }
        fun setSecure(isSecure: Boolean) = apply { config.isSecure = isSecure }
        fun setPreferenceMode(mode: Int) = apply { config.preferenceMode = mode }
        fun ok() = SharePrefUtil(context, config)
    }
}