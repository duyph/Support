package org.akd.support.widget.activity

import android.content.Intent
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import org.akd.support.util.network.NetworkMonitors
import org.akd.support.util.network.OnConnectivityListener

abstract class BaseActivity : AppCompatActivity(), OnConnectivityListener {
    private val mNetworkMonitor: NetworkMonitors by lazy {
        NetworkMonitors.getInstance(this)
    }
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        mNetworkMonitor.registerWithLifecycle(this, this)
        getArgumentFromIntent(intent)
        initViews()
    }

    open fun initViews() {}
    open fun getArgumentFromIntent(intent: Intent?) {}
    override fun onConnectivityChanged(isOnline: Boolean) = Unit

    fun replaceFragment(container: Int, fragment: Fragment) {
        // Create new fragment and transaction
        val transaction = supportFragmentManager.beginTransaction()
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(container, fragment)
        transaction.addToBackStack(null)
        // Commit the transaction
        transaction.commit()
    }

    @get:DrawableRes
    open val iconResourceDialog = 0
}